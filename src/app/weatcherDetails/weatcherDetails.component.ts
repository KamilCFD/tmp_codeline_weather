import {
    Component,
    OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { WeatcherService } from '../../services/weather.service';
import { SearchItem } from '../../models/searchItem';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/first';
import { Url } from '../../tools/url';

@Component({  
    selector: 'weatcherDetails',  // <home></home>  
    providers: [ WeatcherService ],
    styleUrls: [ './weatcherDetails.component.css' ],
    templateUrl: './weatcherDetails.component.html'
})
export class WeatcherDetailsComponent implements OnInit {

    public cities: string[] = ['Istanbul', 'Berlin', 'London', 'Helsinki', 'Dublin', 'Vancouver'];
    public city: any;
    public loading = true;
  
    // tslint:disable-next-line:no-empty
    constructor(
        private router: Router, 
        private activatedRoute: ActivatedRoute,  
        public weatcherService: WeatcherService
    ) {}
  
    public async ngOnInit() {
        
        let params = await this.activatedRoute.params.first().toPromise();    
        this.city = await this.weatcherService.location(params.woeid);            
    
        this.loading = false;
        console.log('city', this.city);
        
    }

    public back() {
        this.router.navigateByUrl('/');
    }

    public dayName(day) {
        let d = new Date(day.applicable_date);
        let dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][d.getDay()];
        return dayName;
    }
  
    public temp(day) {        
        return day.the_temp.toFixed(0);
    }

    public maxTemp(day) {        
        return day.max_temp.toFixed(0);
    }

    public minTemp(day) {        
        return day.min_temp.toFixed(0);
    }

    public iconUrl(day) {        
        return Url.iconsBasePath( day.weather_state_abbr); 
    }

}
