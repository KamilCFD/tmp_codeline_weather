import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { WeatcherDetailsComponent } from './weatcherDetails/weatcherDetails.component';
import { SearchWeatherComponent } from './searchWeather/searchWeather.component';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },  
  { path: 'weather/:woeid',  component: WeatcherDetailsComponent },  
  { path: 'search/:keyword',  component: SearchWeatherComponent },  
  { path: '**',    component: NoContentComponent },
];
