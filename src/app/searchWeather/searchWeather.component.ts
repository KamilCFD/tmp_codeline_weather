import {
    Component,
    OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { WeatcherService } from '../../services/weather.service';
import { SearchItem } from '../../models/searchItem';
import { Router, ActivatedRoute } from '@angular/router';

@Component({  
    selector: 'search-weather',  // <home></home>  
    providers: [ WeatcherService ],
    styleUrls: [ './searchWeather.component.css' ],
    templateUrl: './searchWeather.component.html'
})
export class SearchWeatherComponent implements OnInit {

    public searchItems = [];
    public data: any[] = [];
    public loadedCities = 0;
    public query = "";
    public sub = null;
  
    // tslint:disable-next-line:no-empty
    constructor(
      private router: Router, 
      private activatedRoute: ActivatedRoute,  
      public weatcherService: WeatcherService
    ) {}
  
    public async ngOnInit() {
        console.log('hello `Home` component');
        this.sub = this.activatedRoute.params.subscribe( (params) => {
            this.query = params.keyword;
            this.reload();
          });
        
    }
  
    public async reload() {
        //let params = await this.activatedRoute.params.first().toPromise();    
        this.data = [];
        this.loadedCities = 0;
        this.weatcherService.search(this.query ).subscribe( async (searchItems: any[]) => {   
            this.searchItems = searchItems;                                      
            for (let searchItem of searchItems) {
                let city = await this.weatcherService.location(searchItem.woeid);
                let entry = { city, searchItem };
                this.data.push(entry);
                this.loadedCities++;
            }
        });
        
    }

    public noResults() {
        return this.searchItems.length <= 0;
    }

    public isLoading() {
        if (this.loadedCities >= this.searchItems.length) {
          return false;
        }
        return true;
    }

    public weatherClick(event) {
      this.router.navigateByUrl('weather/' + event.data.searchItem.woeid);
    }

    public sendQuery(query) {
        this.router.navigateByUrl('search/' + encodeURIComponent(query));
      }
}
