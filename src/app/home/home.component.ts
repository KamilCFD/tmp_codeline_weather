import {
    Component,
    OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { WeatcherService } from '../../services/weather.service';
import { SearchItem } from '../../models/searchItem';
import { Router, ActivatedRoute } from '@angular/router';

@Component({  
    selector: 'home',  // <home></home>  
    providers: [ WeatcherService ],
    styleUrls: [ './home.component.css' ],
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    public cities: string[] = ['Istanbul', 'Berlin', 'London', 'Helsinki', 'Dublin', 'Vancouver'];
    public data: any[] = [];
    public loadedCities = 0;
    
    // tslint:disable-next-line:no-empty
    constructor(
      private router: Router, 
      private activatedRoute: ActivatedRoute,  
      public weatcherService: WeatcherService
    ) {}
  
    public async ngOnInit() {
        console.log('hello `Home` component');
        this.reload();
    }
  
    public async reload() {
        for (let cityItem of this.cities) {
            this.weatcherService.search(cityItem).subscribe( async (searchItem: any) => {                              
                let city = await this.weatcherService.location(searchItem[0].woeid);
                let entry = { city, searchItem: searchItem[0] };
                this.data.push(entry);
                this.loadedCities++;
                console.log(this.data);
            });
        }
    }

    public isLoading() {
        if (this.loadedCities >= this.cities.length) {
          return false;
        }
        return true;
    }

    public weatherClick(event) {
      this.router.navigateByUrl('weather/' + event.data.searchItem.woeid);
    }

    public sendQuery(query) {
      this.router.navigateByUrl('search/' + encodeURIComponent(query));
    }
}
