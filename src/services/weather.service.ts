import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { SearchItem } from '../models/searchItem';
import { Url } from '../tools/url';
import { Obj } from '../tools/obj';

@Injectable()
export class WeatcherService {

    constructor(private http: HttpClient) { }

    /**
     *
     * @param lang for order by name (json) column
     */
    public search(name: string) {
        return this.http.get(Url.api(`weather.php?command=search&keyword=${name}`))
            .map( (response) => {
                return Obj.castArr(response, SearchItem);
            } );
    }

    public async location(woeid: number ) {
        return this.http.get(Url.api(`weather.php?command=location&woeid=${woeid}`))
            .map( (response) => {
                return Obj.castArr(response, SearchItem);
            }).toPromise();
    }
}
