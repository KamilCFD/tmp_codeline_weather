import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'loader',
    providers: [ ],    
    styleUrls: [ './loader.component.css' ],
    templateUrl: './loader.component.html'
})
export class LoaderComponent
{
    @Input() public size = '100px'; // '50vmin'; 
}
