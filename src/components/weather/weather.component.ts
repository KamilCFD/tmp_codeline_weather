import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { WeatcherService } from '../../services/weather.service';
import { SearchItem } from '../../models/searchItem';
import { Url } from '../../tools/url';

@Component({  
    selector: 'weather',  // <weather></weather>  
    providers: [ WeatcherService ],
    styleUrls: [ './weather.component.css' ],
    templateUrl: './weather.component.html'
})
export class WeatherComponent implements OnInit {

    @Input() public data: any;
    @Output() public click = new EventEmitter<any>();

    public iconUrlBase = 'https://www.metaweather.com/static/img/weather/';

    // public icons = {
    //     sn: "https://www.metaweather.com/static/img/weather/sn.svg",
    //     sl: "https://www.metaweather.com/static/img/weather/sl.svg",
    //     h: ""
    //     t: ""
    //     hr: ""
    //     lr: ""
    //     s: ""
    //     hc: ""
    //     lc: ""
    //     c: ""  
    // }
  
    public async ngOnInit() {        
        console.log(this.data);
    }

    public temp() {        
        return this.data.city.consolidated_weather[0].the_temp.toFixed(0);
    }

    public maxTemp() {        
        return this.data.city.consolidated_weather[0].max_temp.toFixed(0);
    }

    public minTemp() {        
        return this.data.city.consolidated_weather[0].min_temp.toFixed(0);
    }

    public iconUrl() {        
        //return this.iconUrlBase + this.data.city.consolidated_weather[0].weather_state_abbr + '.svg'; 
        return Url.iconsBasePath( this.data.city.consolidated_weather[0].weather_state_abbr );
    }

    public clicked(event) {
        this.click.emit({ data: this.data, event });
    }

    // public temp() {        
    //     return this.avg(this.data.city.consolidated_weather, 'the_temp');
    // }

    // public maxTemp() {        
    //     return this.avg(this.data.city.consolidated_weather, 'max_temp');
    // }

    // public minTemp() {        
    //     return this.avg(this.data.city.consolidated_weather, 'min_temp');
    // }

    // public iconUrl() {
    //     let list = this.data.city.consolidated_weather.map( (c) => c.weather_state_abbr );
    //     console.log({list});
    //     let top = this.findTopOccurances(list);
    //     console.log({top});
    //     return this.iconUrlBase + top + '.svg'; 
    // }

    // private findTopOccurances(a) {
    //     let b = {};
    //     let max = '';
    //     let maxi = 0;

    //     for (let k of a) {            
    //         if ( b[k] ) { b[k]++; } else { b[k] = 1; }                        
    //         if (maxi < b[k]) { max = k; maxi = b[k]; }
    //     }
    //     return max;
    // }

    // private avg(list: any[], property: string) {
    //     let sumT = 0;
    //     let num = list.length;

    //     for (let d of list ){
    //         sumT += d[property];
    //     }

    //     let avgT = sumT / num;
    //     return avgT.toFixed(0);
    // }

}
