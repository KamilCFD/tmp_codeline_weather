import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter
} from '@angular/core';

@Component({  
    selector: 'search',  // <weather></weather>  
    providers: [  ],
    styleUrls: [ './search.component.css' ],
    templateUrl: './search.component.html'
})
export class SearchComponent {

    @Input() public data: any;
    @Output() public search = new EventEmitter<any>();
    public query = '';
  
    public sendQuery() {
        this.search.emit(this.query);
    }


}
