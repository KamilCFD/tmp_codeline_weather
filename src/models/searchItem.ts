
export class SearchItem {

    constructor(
        public title: string,         // "Berlin",
        public location_type: string, // "City"
        public woeid: number,         // 638242
        public latt_long: string,     // "52.516071,13.376980"        
    ) {}
}
