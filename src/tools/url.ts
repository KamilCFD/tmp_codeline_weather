
/**
 * This class is mainly for provide named routes
 */
export class Url {

    public static api(path) {
        return 'http://tmp-codeline-weather-mw.localhost/' + path;
    }

    public static iconsBasePath(code) {
        let iconUrlBase = 'https://www.metaweather.com/static/img/weather/';
        return iconUrlBase + code + '.svg'; 
    }
}
