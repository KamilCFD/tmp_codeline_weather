/**
 * This class is mainly for provide named routes
 */
export class Obj {

    // this injector is used to get instances of angular services in static class
    public static angularInjector = null;

    /**
     * This method allow to cast json (and not only) obiects to given type including methods
     * Example usage:
     * 
     * CAUTION: Constructor of 'type' T of object obj is never call during casting
     *
     * let space: Space = this.cast(spaceFromJson,Space);
     *
     * (we use type: { new(...args): T} tp creeate fat arrow functions 
     * in prototpye...  https://stackoverflow.com/a/32186367/860099)
     *
     * @param obj object (from json) that has only fields and no methods
     * @param type desired type
     * @returns {any} object that hava fields from obj and methods from type
     */
    public static cast<T>(obj, type: { new(...args): T} ): T {
        obj.__proto__ = type.prototype;
        return obj;
    }

    /**
     * This method allow to cast json (and not only) obiects to given type including methods
     * Example usage:
     *
     * let spaces: Space[] = this.cast(spacesArrayFromJson,Space);
     * (we use type: { new(...args): T} tp creeate fat arrow functions in prototpye...  
     * https://stackoverflow.com/a/32186367/860099)
     *
     * @param objArr array of objects (from json) that have only fields and no methods
     * @param type desired type
     * @returns {any} object that hava fields from obj and methods from type
     */
    public static castArr<T>(objArr, type: { new(...args): T} ): T[] {
        for (const element of objArr) {
            element.__proto__ = type.prototype;
        }
        return objArr;
    }
}
